export default {
    httpPort: process.env.APP_HTTP_PORT,
    publicUrl: process.env.APP_PUBLIC_URL,
    environmentName: process.env.APP_ENVIRONMENT_NAME || 'local',
    log: {
        level: process.env.APP_LOG_LEVEL || 'debug',
    },
    pgDb: {
        database: process.env.APP_PG_DATABASE,
        host: process.env.APP_PG_HOST,
        pass: process.env.APP_PG_PASS,
        port: process.env.APP_PG_PORT,
        user: process.env.APP_PG_USER,
    },
    jwt: {
        ttl: process.env.APP_JWT_TTL,
        password: process.env.APP_JWT_PASSWORD,
    },
};
