import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Oauthclient {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    clientId: string;

    @Column()
    clientSecret: string;

    @Column()
    grantType: string;
}