import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Todolist } from './Todolist';

@Entity()
export class Task {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    status: string;

    @ManyToOne(() => Todolist, todolist => todolist.tasks)
    todolist: Todolist;
}