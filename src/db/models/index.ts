import { Oauthclient } from './Oauthclient';
import { Task } from './Task';
import { Todolist } from './Todolist';

export default {
    Oauthclient,
    Task,
    Todolist
};
