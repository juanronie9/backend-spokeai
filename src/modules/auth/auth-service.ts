import applicationConfig from '../../config/application';
import jwt from 'jsonwebtoken';
import fs from 'fs';
import path from 'path';
import { Oauthclient } from '../../db/models/Oauthclient';
import { getRepository } from 'typeorm';
import { logger } from '../../app';

export const TOKEN_TTL_MS = 30 * 60 * 1000; // 30 minutes

export interface TokenPayload {
    clientId: string;
}

export class AuthService {

    private readonly _certPrivatePath: string;
    private readonly _certPublicPath: string;
    private readonly _tokenTTL: number;
    private readonly _certFile: any;
    private readonly _publicCertFile: any;
    private readonly _certPassword: string;

    constructor() {

        // init default service values
        this._certPrivatePath = path.resolve(__dirname, '../../config/key/token.private.pem');
        this._certPublicPath = path.resolve(__dirname, '../../config/key/token.public.pem');
        this._certPassword = applicationConfig.jwt.password;
        this._tokenTTL =  TOKEN_TTL_MS;

        try {
            const cert = fs.readFileSync(this._certPrivatePath, 'utf8');
            const publiCcert = fs.readFileSync(this._certPublicPath, 'utf8');
            this._certFile = cert;
            this._publicCertFile = publiCcert;
        } catch (e) {
            logger.error('Error init auth service');
            throw new Error(e);
        }
    }

    obtainTokenFromRequest(req, headerKeys, queryKey): string {
        let token = '';
        if (req.query && req.query[queryKey]) {
            return req.query[queryKey];
        }
        if (req.headers) {
            for (const headerKey of headerKeys) {
                if (req.headers[headerKey]) {
                    token = req.headers[headerKey];
                    break;
                }
            }
        }
        return token;
    }

    async validateToken(token: string): Promise<boolean> {
        let payload: TokenPayload;

        try {
            payload = await jwt.verify(token, this._publicCertFile, { algorithms: ['RS256'] });
            return !!(payload);
        } catch (e) {
            return false;
        }
    }

    async createToken(payload: TokenPayload): Promise<string> {
        const token = jwt.sign(payload, { key: this._certFile, passphrase: this._certPassword }, { expiresIn: this._tokenTTL, algorithm: 'RS256' });
        return token;
    }

    async validateOAuthclient(clientId: string, clientSecret: string, grantType: string): Promise<Oauthclient> {
        const oauthclient = await getRepository(Oauthclient).findOne({ clientId, clientSecret, grantType });
        if (!oauthclient) {
            return null;
        }
        return oauthclient;
    }
}
