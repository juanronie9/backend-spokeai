import { Interactor, IRequestObject } from '../core';
import joi from 'joi';
import { logger } from '../../app';
import { AuthService, TokenPayload } from './auth-service';

export interface GetOAuthTokenRequestObject extends IRequestObject {
    grant_type: string,
    client_id: string,
    client_secret: string;
}

export const validationSchema = joi.object().keys({
    grant_type: joi
        .string()
        .required(),
    client_id: joi
        .string()
        .required(),
    client_secret: joi
        .string()
        .required()
});

export class GetOauthtokenInteractor extends Interactor {

    async execute(request: GetOAuthTokenRequestObject) {
        try {
            const authService = new AuthService();
            const oauthClient = await authService.validateOAuthclient(request.client_id, request.client_secret, request.grant_type);
            if (!oauthClient) {
                return this.presenter.error([ 'OAuth client not found' ]);
            }
            const payload: TokenPayload = {
                clientId: oauthClient.clientId,
            }
            const accessToken = await authService.createToken(payload);

            const result = {
                access_token: accessToken
            }
            return this.presenter.success(result);
        } catch (e) {
            logger.error('Failed to get access token', request, e);
            return this.presenter.error([e.message]);
        }
    }
}