import { getConnectionManager, /* ConnectionManager, Connection */ } from 'typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { Task } from '../../db/models/Task';
import { Oauthclient } from '../../db/models/Oauthclient';
import { Todolist } from '../../db/models/Todolist';

export class PgsqlConnector {
    _pgsql;

    setConfig(value) {
        this._pgsql = value;
        return this;
    }

    async connect() {
        const { database, host, user, pass, port } = this._pgsql;
        const connectionManager = getConnectionManager();
        const connection = connectionManager.create({
            type: 'postgres',
            host,
            port,
            username: user,
            password: pass,
            database,
            entities: [
                Task,
                Oauthclient,
                Todolist
            ],
            synchronize: true,
            logging: false,
            namingStrategy: new SnakeNamingStrategy(),
        });
        await connection.connect(); // performs connection
    }
}
