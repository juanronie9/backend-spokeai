import { Interactor, IRequestObject } from '../core';
import joi from 'joi';
import { logger } from '../../app';
import { getRepository } from 'typeorm';
import { Todolist } from '../../db/models/Todolist';

export interface CreateTodolistRequestObject extends IRequestObject {
    name: string,
}

export const validationSchema = joi.object().keys({
    name: joi
        .string()
        .required(),
});

export class CreateTodolistInteractor extends Interactor {

    async execute(request: CreateTodolistRequestObject) {
        try {
            const todolistRepository = getRepository(Todolist);
            const todolist = new Todolist();
            todolist.name = request.name;
            const created = await todolistRepository.save(todolist);

            return await this.presenter.success(created);
        } catch (e) {
            logger.error('Failed to create todolist', request, e);
            return this.presenter.error([e.message]);
        }
    }
}
