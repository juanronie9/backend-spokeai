import { Interactor, IRequestObject } from '../core';
import joi from 'joi';
import { logger } from '../../app';
import { getRepository } from 'typeorm';
import { Task } from '../../db/models/Task';

export interface UpdateTaskRequestObject extends IRequestObject {
    taskId: number,
    name: string,
    status: string,
}

export const validationSchemaParams = joi.object().keys({
    taskId: joi
        .number()
        .required(),
});

export const validationSchema = joi.object().keys({
    name: joi
        .string()
        .required(),
    status: joi
        .string()
        .valid('in-progress','completed')
        .required(),
});

export class UpdateTaskInteractor extends Interactor {

    async execute(request: UpdateTaskRequestObject) {
        try {
            const taskRepository = getRepository(Task);
            const task = await taskRepository.findOne({ id: request.taskId });
            if (!task) {
                return this.presenter.error(['Task not found']);
            }

            task.status = request.status;
            task.name = request.name;
            const updated = await taskRepository.save(task);

            return await this.presenter.success(updated);
        } catch (e) {
            logger.error('Failed to update task', request, e);
            return this.presenter.error([e.message]);
        }
    }
}
