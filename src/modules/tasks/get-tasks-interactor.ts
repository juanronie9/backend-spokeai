import joi from 'joi';
import { IRequestObject, Interactor, PaginationData } from '../core';
import { MAX_PAGE_SIZE, MIN_PAGE_NUMBER, MIN_PAGE_SIZE } from '../common/pagination';
import { logger } from '../../app';
import { getRepository } from 'typeorm';
import { Task } from '../../db/models/Task';
import { Todolist } from '../../db/models/Todolist';

export interface GetTasksRequestObject extends IRequestObject {
    todolistId: number;
    page: number;
    pageSize: number;
    status: string;
}

export const validationSchemaParams = joi.object().keys({
    todolistId: joi
        .number()
        .required()
});

export const validationSchema = joi.object().keys({
    page: joi
        .number()
        .integer()
        .min(MIN_PAGE_NUMBER)
        .default(MIN_PAGE_NUMBER)
        .optional(),
    pageSize: joi
        .number()
        .integer()
        .min(MIN_PAGE_SIZE)
        .max(MAX_PAGE_SIZE)
        .default(MIN_PAGE_SIZE)
        .optional(),
    status: joi
        .string()
        .valid('in-progress','completed')
        .optional(),
});

export class GetTasksInteractor extends Interactor {

    async execute(request: GetTasksRequestObject) {
        try {
            const todolist = await getRepository(Todolist).findOne({ id: request.todolistId });
            if (!todolist) {
                return this.presenter.error(['Todolist not found']);
            }

            const result = {
                rows: [],
                count: 0,
            };

            const query: any = {};
            query.todolist = todolist.id;
            if (request.status) {
                query.status = request.status
            }
            const data = await getRepository(Task).findAndCount({
                where: query,
                skip: request.pageSize * (request.page - 1),
                take: request.pageSize
            });
            result.rows = data[0];
            result.count = data[1];

            const pagination = this._buildPaginationData(
                result.count,
                request.pageSize,
                request.page,
            );

            return await this.presenter.success(result, pagination);
        } catch (error) {
            logger.error('Failed to get tasks', {
                errorMessage: error.message,
                ...request
            });
            return this.presenter.error([error]);
        }
    }

    private _buildPaginationData(
        count: number,
        pageSize: number,
        page: number,
    ): PaginationData {
        const totalPages = count > 0 ? Math.ceil(count / pageSize) : 0;

        const paginationData = {
            totalPages: Number(totalPages),
            page,
            pageSize,
        };

        return paginationData;
    }
}
