import { Interactor, IRequestObject } from '../core';
import joi from 'joi';
import { logger } from '../../app';
import { getRepository } from 'typeorm';
import { Task } from '../../db/models/Task';
import { Todolist } from '../../db/models/Todolist';

export interface CreateTaskRequestObject extends IRequestObject {
    todolistId: number,
    name: string,
    status: string,
}

export const validationSchemaParams = joi.object().keys({
    todolistId: joi
        .number()
        .required(),
});

export const validationSchema = joi.object().keys({
    name: joi
        .string()
        .required(),
    status: joi
        .string()
        .valid('in-progress','completed')
        .required(),
});

export class CreateTaskInteractor extends Interactor {

    async execute(request: CreateTaskRequestObject) {
        try {
            const todolist = await getRepository(Todolist).findOne({ id: request.todolistId });
            if (!todolist) {
                return this.presenter.error(['Todolist not found']);
            }

            const task = new Task();
            task.status = request.status;
            task.name = request.name;
            task.todolist = todolist;
            const created = await getRepository(Task).save(task);

            return await this.presenter.success(created);
        } catch (e) {
            logger.error('Failed to create task', request, e);
            return this.presenter.error([e.message]);
        }
    }
}
