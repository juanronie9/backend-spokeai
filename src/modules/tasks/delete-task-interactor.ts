import { Interactor, IRequestObject } from '../core';
import joi from 'joi';
import { logger } from '../../app';
import { getRepository } from 'typeorm';
import { Task } from '../../db/models/Task';

export interface DeleteTaskRequestObject extends IRequestObject {
    taskId: number,
}

export const validationSchemaParams = joi.object().keys({
    taskId: joi
        .number()
        .required(),
});

export class DeleteTaskInteractor extends Interactor {

    async execute(request: DeleteTaskRequestObject) {
        try {
            const result = await getRepository(Task).delete({ id: request.taskId });
            if (!result.affected) {
                return this.presenter.error(['Task not found']);
            }

            return await this.presenter.success({ message: 'deleted'});
        } catch (e) {
            logger.error('Failed to delete task', request, e);
            return this.presenter.error([e.message]);
        }
    }
}
