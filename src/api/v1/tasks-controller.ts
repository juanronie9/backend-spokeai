import {
    ExpressController,
    ExpressJSONCreatePresenter,
    ExpressJSONDeletePresenter, ExpressJSONSearchPresenter,
    ExpressJSONUpdatePresenter,
} from '../../modules/core';
import { AuthMiddleware } from '../../modules/core/auth.middleware';
import {
    GetTasksInteractor,
    GetTasksRequestObject,
    validationSchema as getAllSchema,
    validationSchemaParams as getSchemaParams
} from '../../modules/tasks/get-tasks-interactor';
import {
    CreateTaskInteractor,
    CreateTaskRequestObject,
    validationSchema as createSchema,
    validationSchemaParams as createSchemaParams
} from '../../modules/tasks/create-task-interactor';
import {
    UpdateTaskInteractor,
    UpdateTaskRequestObject,
    validationSchema as updateSchema,
    validationSchemaParams as updateSchemaParams
} from '../../modules/tasks/update-task-interactor';
import {
    DeleteTaskInteractor,
    DeleteTaskRequestObject,
    validationSchemaParams as deleteSchemaParams
} from '../../modules/tasks/delete-task-interactor';

export class TasksController extends ExpressController {
    constructor() {
        super();

        this.router.get(
            '/todolist/:todolistId/',
            AuthMiddleware,
            this.validator.validateParams(getSchemaParams),
            this.validator.validateQuery(getAllSchema),
            this.getTasks.bind(this)
        );
        this.router.post(
            '/todolist/:todolistId/',
            AuthMiddleware,
            this.validator.validateParams(createSchemaParams),
            this.validator.validateBody(createSchema),
            this.createTask.bind(this)
        );
        this.router.put('/:taskId',
            AuthMiddleware,
            this.validator.validateParams(updateSchemaParams),
            this.validator.validateBody(updateSchema),
            this.updateTask.bind(this)
        );
        this.router.delete('/:taskId',
            AuthMiddleware,
            this.validator.validateParams(deleteSchemaParams),
            this.deleteTask.bind(this)
        );
    }

    async getTasks(req, res, next) {
        const interactor = new GetTasksInteractor();
        const presenter = new ExpressJSONSearchPresenter(req, res, next);

        const request = req.query as GetTasksRequestObject;
        request.todolistId = req.params.todolistId;

        interactor.setPresenter(presenter);
        await interactor.execute(request);
    }

    async createTask(req, res, next) {
        const interactor = new CreateTaskInteractor();
        const presenter = new ExpressJSONCreatePresenter(req, res, next);

        const request = req.body as CreateTaskRequestObject;
        request.todolistId = req.params.todolistId;

        interactor.setPresenter(presenter)

        await interactor.execute(request);
    }

    async updateTask(req, res, next) {
        const interactor = new UpdateTaskInteractor();
        const presenter = new ExpressJSONUpdatePresenter(req, res, next);

        const request = req.body as UpdateTaskRequestObject;
        request.taskId = req.params.taskId;

        interactor.setPresenter(presenter)

        await interactor.execute(request);
    }

    async deleteTask(req, res, next) {
        const interactor = new DeleteTaskInteractor();
        const presenter = new ExpressJSONDeletePresenter(req, res, next);

        const request = req.params as DeleteTaskRequestObject;
        request.taskId = req.params.taskId;

        interactor.setPresenter(presenter)

        await interactor.execute(request);
    }
}
