import { Router } from 'express';
import { TasksController } from './tasks-controller';
import { AuthController } from './auth-controller';
import { TodolistsController } from './todolists-controller';

export class ApiRouter {

    constructor() {}

    build() {
        const router = Router();

        const authController = new AuthController();
        const todolistsController = new TodolistsController();
        const tasksController = new TasksController();

        router.get('/healthcheck', (req, res) => {
            const healthcheck = {
                uptime: process.uptime(),
                message: 'OK',
                timestamp: Date.now()
            };
            try {
                return res.status(200).send(healthcheck);
            } catch (e) {
                healthcheck.message = e;
                return res.status(503).send();
            }
        });

        // Auth api
        router.use('/oauth', authController.router);

        // Todolists api
        router.use('/todolists', todolistsController.router);

        // Tasks api
        router.use('/tasks', tasksController.router);

        return router;
    }
}
