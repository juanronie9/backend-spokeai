import {
    ExpressController,
    ExpressJSONCreatePresenter,
} from '../../modules/core';
import { CreateTodolistInteractor, CreateTodolistRequestObject, validationSchema as createSchema } from '../../modules/todolists/create-todolist-interactor';
import { AuthMiddleware } from '../../modules/core/auth.middleware';

export class TodolistsController extends ExpressController {
    constructor() {
        super();

        this.router.post('/', AuthMiddleware, this.validator.validateBody(createSchema), this.createTodolist.bind(this));
    }

    async createTodolist(req, res, next) {
        const interactor = new CreateTodolistInteractor();
        const presenter = new ExpressJSONCreatePresenter(req, res, next);

        const request = req.body as CreateTodolistRequestObject;

        interactor.setPresenter(presenter)

        await interactor.execute(request);
    }
}
