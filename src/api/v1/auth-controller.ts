import { ExpressController, ExpressJSONShowPresenter } from '../../modules/core';
import {
    GetOauthtokenInteractor,
    GetOAuthTokenRequestObject,
    validationSchema as createTokensSchema
} from '../../modules/auth/get-oauthtoken-interactor';

export class AuthController extends ExpressController {
    constructor() {
        super();

        this.router.post('/token', this.validator.validateBody(createTokensSchema), this.getOAuthToken.bind(this));
    }

    async getOAuthToken(req, res, next) {
        const interactor = new GetOauthtokenInteractor();
        const presenter = new ExpressJSONShowPresenter(req, res, next);

        const request = req.body as GetOAuthTokenRequestObject;

        interactor.setPresenter(presenter);

        await interactor.execute(request);
    }
}
