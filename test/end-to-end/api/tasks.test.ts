import applicationConfig from '../../../src/config/application';
import supertest from 'supertest';
import _ from 'lodash';
import { PgsqlConnector } from '../../../src/modules/common/pgsql-connector';
import { getRepository } from 'typeorm';
import { Oauthclient } from '../../../src/db/models/Oauthclient';
import { Todolist } from '../../../src/db/models/Todolist';
import { Task } from '../../../src/db/models/Task';

const prefix = '/v1/';

function api(path) {
    return prefix + path;
}

let request;
let todolistId;

const credentials = {
    'access-token': '',
};
const bodyRequest = {
    client_id: 'test_2_3nio9v1agigw844kssgk4kswksgs0wk4okkoksg4080osoos84',
    client_secret: 'test_4394ff69idq8g08k4wso4ck8008w0g44ck88wo8c8cw80wk8c4',
    grant_type: 'client_credentials',
};

const bodyRequestTask = {
    'name': 'test',
    'status': 'in-progress'
}

// NOTE: Simulated data, in a real application use this flow for every test:
// 1. Insert data in db
// 2. Get data from db
// 3. Verify data from db
// 4. Clear data from db

describe('TasksApiTest', () => {
    beforeAll(async () => {
        const connector = new PgsqlConnector();
        connector.setConfig(applicationConfig.pgDb);
        await connector.connect();

        request = supertest('http://0.0.0.0:3000');

        // INIT DATA
        const oauthClientRepository = getRepository(Oauthclient);
        const data = await oauthClientRepository.findOne(
            { where: { clientId: bodyRequest.client_id, clientSecret: bodyRequest.client_secret, grantType: bodyRequest.grant_type } }
        );
        if (!data) {
            const oauthclient = new Oauthclient();
            oauthclient.clientId = bodyRequest.client_id;
            oauthclient.clientSecret = bodyRequest.client_secret;
            oauthclient.grantType = bodyRequest.grant_type;
            await oauthClientRepository.save(oauthclient);
        }

        // Get access token
        const response = await request.post(api('oauth/token')).send(bodyRequest);
        credentials['access-token'] = response.body.access_token;

        // Create todolist and init
        const todolistResponse = await getRepository(Todolist).findOne();
        if (!todolistResponse) {
            const todolist = new Todolist();
            todolist.name = 'test';
            await getRepository(Todolist).save(todolist);
        }
        const todolistInit = await getRepository(Todolist).findOne();
        todolistId = todolistInit.id;
    });
    afterAll(async () => {
        // await db.close()
    });

    describe('Create task [POST] /v1/tasks/todolist/:todolistId/', () => {
        it('should fail when credentials not provided', async () => {
            const response = await request
                .post(api(`tasks/todolist/${todolistId}/`))
                .send({});

            expect(response.statusCode).toBe(401);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when credentials not valid', async () => {
            const response = await request
                .post(api(`tasks/todolist/${todolistId}/`))
                .set({
                    'access-token': '---'
                })
                .send({});

            expect(response.statusCode).toBe(401);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when path param is not valid: todolistId not valid', async () => {
            const bodyRequestTest = _.clone(bodyRequestTask);
            const response = await request
                .post(api(`tasks/todolist/999/`))
                .set(credentials)
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when body is not valid: parameters required missing', async () => {
            const bodyRequestTest = _.clone(bodyRequestTask);
            delete bodyRequestTest.name;
            const response = await request
                .post(api(`tasks/todolist/${todolistId}/`))
                .set(credentials)
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when body is not valid: parameters type not valid', async () => {
            const bodyRequestTest = _.clone(bodyRequestTask);
            bodyRequestTest.status = '---';
            const response = await request
                .post(api(`tasks/todolist/${todolistId}/`))
                .set(credentials)
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should success', async () => {
            const bodyRequestTest = _.clone(bodyRequestTask);
            const response = await request
                .post(api(`tasks/todolist/${todolistId}/`))
                .set(credentials)
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(201);
            const { body } = response;
            expect(body.id).not.toBeNull();

            // -----------------
            // Verification data
            // -----------------

            // 1. Verify created tasks exist
            const task = await getRepository(Task).findOne({ id: body.id });
            expect(task.id).toBe(body.id);
            expect(task.name).toBe(body.name);
            expect(task.status).toBe(body.status);
        });
    });

    describe('Get tasks [GET] /v1/tasks/todolist/:todolistId/', () => {
        it('should fail when credentials not provided', async () => {
            const response = await request
                .get(api(`tasks/todolist/${todolistId}/`))
                .send({});

            expect(response.statusCode).toBe(401);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when credentials not valid', async () => {
            const response = await request
                .get(api(`tasks/todolist/${todolistId}/`))
                .set({
                    'access-token': '---'
                })
                .send({});

            expect(response.statusCode).toBe(401);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when path param is not valid: todolistId not valid', async () => {
            const response = await request
                .get(api(`tasks/todolist/999/`))
                .set(credentials)
                .send();

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when query is not valid: parameters type not valid', async () => {
            const response = await request
                .get(api(`tasks/todolist/${todolistId}/`))
                .set(credentials)
                .query({
                    'status': '---'
                })
                .send();

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when query is not valid: parameters type not valid (pagination)', async () => {
            const response = await request
                .get(api(`tasks/todolist/${todolistId}/`))
                .set(credentials)
                .query({
                    'page': '---',
                    'pageSize': '---'
                })
                .send();

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should success: without filters', async () => {
            const response = await request
                .get(api(`tasks/todolist/${todolistId}/`))
                .set(credentials)
                .query()
                .send();

            expect(response.statusCode).toBe(200);
            const { body, headers } = response;
            expect(body.rows.length).toBeGreaterThanOrEqual(0);
            expect(headers).toHaveProperty('x-pagination-current-page');
            expect(headers).toHaveProperty('x-pagination-pages');
            expect(headers).toHaveProperty('x-pagination-page-size');
        });

        it('should success: pagination filter', async () => {
            const currentPage = 2;
            const response = await request
                .get(api(`tasks/todolist/${todolistId}/`))
                .set(credentials)
                .query({
                    page: currentPage,
                    pageSize: 10,
                })
                .send();

            expect(response.statusCode).toBe(200);
            const { body, headers } = response;
            expect(body.rows.length).toBeGreaterThanOrEqual(0);
            expect(headers).toHaveProperty('x-pagination-current-page');
            expect(headers).toHaveProperty('x-pagination-pages');
            expect(headers).toHaveProperty('x-pagination-page-size');
        });

        it('should success: status filter', async () => {
            // 1. Insert new task
            const bodyRequestTest = _.clone(bodyRequestTask);
            const response = await request.post(api(`tasks/todolist/${todolistId}/`)).set(credentials).send(bodyRequestTest);
            expect(response.statusCode).toBe(201);

            // 2. Get tasks by status
            const responseTasks = await request
                .get(api(`tasks/todolist/${todolistId}/`))
                .set(credentials)
                .query({
                    status: bodyRequestTest.status
                })
                .send();

            expect(responseTasks.statusCode).toBe(200);
            const { body } = responseTasks;
            expect(body.rows.length).toBeGreaterThanOrEqual(1);
        });
    });

    describe('Update task [PUT] /v1/tasks/:taskId/', () => {
        it('should fail when credentials not provided', async () => {
            const taskId = 1;
            const response = await request
                .put(api(`tasks/${taskId}/`))
                .send({});

            expect(response.statusCode).toBe(401);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when credentials not valid', async () => {
            const taskId = 1;
            const response = await request
                .put(api(`tasks/${taskId}/`))
                .set({
                    'access-token': '---'
                })
                .send({});

            expect(response.statusCode).toBe(401);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when path param is not valid: taskId not valid', async () => {
            const bodyRequestTest = _.clone(bodyRequestTask);
            const response = await request
                .put(api(`tasks/999/`))
                .set(credentials)
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when body is not valid: parameters required missing', async () => {
            const taskId = 1;
            const bodyRequestTest = _.clone(bodyRequestTask);
            delete bodyRequestTest.name;
            const response = await request
                .put(api(`tasks/${taskId}/`))
                .set(credentials)
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when body is not valid: parameters type not valid', async () => {
            const taskId = 1;
            const bodyRequestTest = _.clone(bodyRequestTask);
            bodyRequestTest.status = '---';
            const response = await request
                .put(api(`tasks/${taskId}/`))
                .set(credentials)
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should success', async () => {
            // 1. Create new task
            const bodyRequestTest = _.clone(bodyRequestTask);
            const responseTask = await request.post(api(`tasks/todolist/${todolistId}/`)).set(credentials).send(bodyRequestTest);
            const taskId = responseTask.body.id;

            // 2. Update task
            const bodyRequestUpdateTest = _.clone(bodyRequestTask);
            bodyRequestUpdateTest.name = 'updated task';
            bodyRequestUpdateTest.status = 'completed';
            const response = await request
                .put(api(`tasks/${taskId}/`))
                .set(credentials)
                .send(bodyRequestUpdateTest);

            expect(response.statusCode).toBe(200);
            const { body } = response;
            expect(body.id).toBe(taskId);
            expect(body.name).toBe(bodyRequestUpdateTest.name);
            expect(body.status).toBe(bodyRequestUpdateTest.status);

            // -----------------
            // Verification data
            // -----------------

            // 1. Verify created tasks exist
            const task = await getRepository(Task).findOne({ id: body.id });
            expect(task.id).toBe(body.id);
            expect(task.name).toBe(body.name);
            expect(task.status).toBe(body.status);
        });
    });

    describe('Delete task [DELETE] /v1/tasks/:taskId/', () => {
        it('should fail when credentials not provided', async () => {
            const taskId = 1;
            const response = await request
                .delete(api(`tasks/${taskId}/`))
                .send({});

            expect(response.statusCode).toBe(401);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when credentials not valid', async () => {
            const taskId = 1;
            const response = await request
                .delete(api(`tasks/${taskId}/`))
                .set({
                    'access-token': '---'
                })
                .send({});

            expect(response.statusCode).toBe(401);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when path param is not valid: taskId not valid', async () => {
            const response = await request
                .delete(api(`tasks/999/`))
                .set(credentials)
                .send();

            expect(response.statusCode).toBe(406);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should success', async () => {
            // 1. Create new task
            const bodyRequestTest = _.clone(bodyRequestTask);
            const responseTask = await request.post(api(`tasks/todolist/${todolistId}/`)).set(credentials).send(bodyRequestTest);
            const taskId = responseTask.body.id;

            // 2. Delete task
            const response = await request
                .delete(api(`tasks/${taskId}/`))
                .set(credentials)
                .send();

            expect(response.statusCode).toBe(200);
            const { body } = response;
            expect(body.message).toBe('deleted');

            // -----------------
            // Verification data
            // -----------------

            // 1. Verify deleted task not exist
            const task = await getRepository(Task).findOne({ id: body.id });
            expect(task).toBeUndefined();
        });
    });
    // ...
    // ...
});
