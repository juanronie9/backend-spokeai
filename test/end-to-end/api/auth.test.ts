import applicationConfig from '../../../src/config/application';
import supertest from 'supertest';
import _ from 'lodash';
import { PgsqlConnector } from '../../../src/modules/common/pgsql-connector';
import { getRepository } from 'typeorm';
import { Oauthclient } from '../../../src/db/models/Oauthclient';
import { AuthService } from '../../../src/modules/auth/auth-service';

const prefix = '/v1/';

function api(path) {
    return prefix + path;
}

let request;

const bodyRequest = {
    client_id: 'test_2_3nio9v1agigw844kssgk4kswksgs0wk4okkoksg4080osoos84',
    client_secret: 'test_4394ff69idq8g08k4wso4ck8008w0g44ck88wo8c8cw80wk8c4',
    grant_type: 'client_credentials',
};

// NOTE: Simulated data, in a real application use this flow for every test:
// 1. Insert data in db
// 2. Get data from db
// 3. Verify data from db
// 4. Clear data from db

describe('AuthApiTest', () => {
    beforeAll(async () => {
        const connector = new PgsqlConnector();
        connector.setConfig(applicationConfig.pgDb);
        await connector.connect();

        request = supertest('http://0.0.0.0:3000');

        // INIT DATA
        const oauthClientRepository = getRepository(Oauthclient);
        const data = await oauthClientRepository.findOne(
            { where: { clientId: bodyRequest.client_id, clientSecret: bodyRequest.client_secret, grantType: bodyRequest.grant_type } }
        );
        if (!data) {
            const oauthclient = new Oauthclient();
            oauthclient.clientId = bodyRequest.client_id;
            oauthclient.clientSecret = bodyRequest.client_secret;
            oauthclient.grantType = bodyRequest.grant_type;
            await oauthClientRepository.save(oauthclient);
        }
    });
    afterAll(async () => {
        // await db.close()
    });

    describe('Get token [POST] /v1/oauth/token', () => {
        it('should fail when body is not provided', async () => {
            const response = await request
                .post(api('oauth/token'))
                .send({});

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when body is not valid: parameters required missing', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            delete bodyRequestTest.client_id;
            const response = await request
                .post(api('oauth/token'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when body is not valid: parameters type not valid', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            bodyRequestTest.client_id = 1234;
            const response = await request
                .post(api('oauth/token'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when data is not valid: oauth client', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            bodyRequestTest.client_id = '-----'
            const response = await request
                .post(api('oauth/token'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(404);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should success', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            const response = await request
                .post(api('oauth/token'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(200);
            const { body } = response;
            expect(body.access_token).not.toBeNull();

            // -----------------
            // Verification data
            // -----------------

            // 1. Verify received access_token exist
            const authService = new AuthService();
            const isValid = await authService.validateToken(body.access_token);
            expect(isValid).toBe(true);
        });
    });
});
