import faker from 'faker';
import applicationConfig from '../../src/config/application';
import { AuthService, TokenPayload } from '../../src/modules/auth/auth-service';
import { PgsqlConnector } from '../../src/modules/common/pgsql-connector';
import { Oauthclient } from '../../src/db/models/Oauthclient';
import { getRepository } from 'typeorm';

const bodyRequest = {
    client_id: 'test_2_3nio9v1agigw844kssgk4kswksgs0wk4okkoksg4080osoos84',
    client_secret: 'test_4394ff69idq8g08k4wso4ck8008w0g44ck88wo8c8cw80wk8c4',
    grant_type: 'client_credentials',
};

describe('AuthService', () => {
    beforeAll(async () => {
        const connector = new PgsqlConnector();
        connector.setConfig(applicationConfig.pgDb);
        await connector.connect();
    })

    afterAll(async () => {
        // await db.close()
    })

    describe('validateToken', () => {
        it('should fail: token not valid', async () => {
            const authService = new AuthService();
            const isValid = await authService.validateToken('');
            expect(isValid).toBe(false);
        });
        it('should return data', async () => {
            // 1. Create OAuth Token
            const payload: TokenPayload = {
                clientId: bodyRequest.client_id,
            }
            const authService = new AuthService();
            const accessToken = await authService.createToken(payload);
            expect(accessToken).not.toBeNull();

            // 2. Validate Token
            const isValid = await authService.validateToken(accessToken);
            expect(isValid).toBe(true);
        });
    });

    describe('validateOAuthclient', () => {
        it('should fail: data not valid', async () => {
            const authService = new AuthService();
            const isValid = await authService.validateOAuthclient(faker.datatype.uuid(), faker.datatype.uuid(), faker.datatype.uuid());
            expect(isValid).toBe(null);
        });
        it('should return data', async () => {
            // 1. Create oauth Client
            const oauthClientRepository = getRepository(Oauthclient);
            const data = await oauthClientRepository.findOne(
                { where: { clientId: bodyRequest.client_id, clientSecret: bodyRequest.client_secret, grantType: bodyRequest.grant_type } }
            );
            if (!data) {
                const oauthclient = new Oauthclient();
                oauthclient.clientId = bodyRequest.client_id;
                oauthclient.clientSecret = bodyRequest.client_secret;
                oauthclient.grantType = bodyRequest.grant_type;
                await oauthClientRepository.save(oauthclient);
            }

            // 2. Validate oauth client
            const authService = new AuthService();
            const isValid = await authService.validateOAuthclient(faker.datatype.uuid(), faker.datatype.uuid(), faker.datatype.uuid());
            expect(isValid).toBeDefined();
        });
    });

    describe('createToken', () => {
        it('should return data', async () => {
            // 1. Create OAuth Token
            const payload: TokenPayload = {
                clientId: bodyRequest.client_id,
            }
            const authService = new AuthService();
            const accessToken = await authService.createToken(payload);
            expect(accessToken).not.toBeNull();

            // 2. Validate Token
            const isValid = await authService.validateToken(accessToken);
            expect(isValid).toBe(true);
        });
    });
});