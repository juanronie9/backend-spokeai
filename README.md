# The Backend API

Provides a rich `backend-api` abilities.

## API

API is built on top of [OpenAPI 3.0 specification](https://swagger.io/specification/)
See: [swagger.yml](./src/templates/api-docs/swagger.yaml) for details.

#### Documentation
```
# DEV
http://localhost:3000/docs/api

# PROD
https://<domain>/docs/api
```

## Building

The project uses [TypeScript](https://github.com/Microsoft/TypeScript) as a JavaScript transpiler.
The sources are located under the `src` directory, the distributables are in the `dist`.

### Requirements:

-   Nodejs
-   PostgreSQL

### Environment:

```bash
APP_HTTP_PORT=3000
APP_PUBLIC_URL=http://localhost:3000
APP_ENVIRONMENT_NAME=
APP_PG_HOST=
APP_PG_PORT=5432
APP_PG_USER=
APP_PG_PASS=
APP_PG_DATABASE=
APP_JWT_TTL=
APP_JWT_PASSWORD=
```

To make the application running. Steps:

0. Create database in your DB host
    ```
    CREATE DATABAE spokeai
    ```   
1. Rename file `.env.example` to `.env`
2. Set Environment
    ```
    set -a
    . .env
    set +a
    ```
3. Init application
    ```bash
    npm run build
    npm run start
    ```
4. Create in postgresql a new oauth client. Example:
    ```
    INSERT INTO oauthclient (client_id, client_secret, grant_type)
    VALUES('2_3nio9v1agigw844kssgk4kswksgs0wk4okkoksg4080osoos84','4394ff69idq8g08k4wso4ck8008w0g44ck88wo8c8cw80wk8c4', 'client_credentials');
    ```
5. Ready! You can test all APIs available. Example from api docs Swagger
    ```
    http://0.0.0.0:3000/docs/api
    ```

## Development

```bash
npm run debug
```

## Testing

It uses Jest and supertest.
```bash
npm run test
```
Open one terminal with project running and other terminal to execute the e2e api tests.
```bash
npm run test:e2e
```
---

## Docker
Init app with docker-compose. Steps:

1. Rename file `.env.docker-compose-example` to `.env`
2. Set Environment 
    ```
    set -a
    . .env
    set +a
    ```
3. Init application
    ```
    docker-compose up -d
    ```
4. Create in postgresql a new oauth client. Example:  
    ```
    INSERT INTO oauthclient (client_id, client_secret, grant_type)
    VALUES('2_3nio9v1agigw844kssgk4kswksgs0wk4okkoksg4080osoos84','4394ff69idq8g08k4wso4ck8008w0g44ck88wo8c8cw80wk8c4', 'client_credentials');
    ```
5. Ready! You can test all APIs available. Example from api docs Swagger
    ```
    http://0.0.0.0:3000/docs/api
    ```


---
Execute tests 
```
docker-compose exec api npm run test:e2e
docker-compose exec api npm run test
```
Rebuild if some change 
```
docker-compose down -v
docker-compose build --no-cache
docker-compose up -d
```


## Application Structure
- [src](./src): The main `backend` application code.
    - [api](./src/api): Defines API routers and controllers
    - [config](./src/config): Defines configuration. Examples ENV_VARS
    - [db](./src/db): MongoDB models and schemas
    - [modules](./src/modules): Defines modules and logic of the application. 
      All the modules are composed of `Interactors` used by controllers, `Gateways` to interact with DB and `Services` to handle logic or request external REST API's.   
- [test](./test): Includes all the testing of the application
- [app.ts](./app.ts): Main file. Inject packages, initiate and bootstrap application


---